# Метод пристрелки

## Постановка задачи

Дана система дифуров
```math
\dot{\mathbf{y}}(t)=\mathbf{f}(\mathbf{y}(t), t, \mathbf{z}),
```

зависящая от параметров $`\mathbf{z}`$ с начальными условиями $`\mathbf{y}_0`$ (параметры могут содержаться в системе уравнений, либо в начальных условиях, но у тебя они сидят в уравнениях).

Необходимо найти такие параметры $`\mathbf{z}_{opt}`$, которые обращают в ноль некую функцию $`\mathbf{r}(\mathbf{x}, \mathbf{z})`$ при $`\mathbf{x}=\mathbf{y}(T)`$.

Определим функцию $`\tilde{\mathbf{y}}(t, \mathbf{y}_0)`$ как результат численного интегрирования системы $`\dot\mathbf{y}=\mathbf{f}`$ от 0 до $`T`$ с начальными условиями $`\mathbf{y}_0`$, тогда для нахождения параметров нужно решить уравнение

```math
\mathbf{g}(\mathbf{z}) = \mathbf{r}(\tilde\mathbf{y}(T, \mathbf{y}_0), \mathbf{z}) = 0
```

относительно вектора $`\mathbf{z}`$. Так как это нелинейное уравнение, то решать будем численным итерационным методом с начальным приближением $`\mathbf{z}_0`$.

## Решение

Сначала мы определяем все константы:

```matlab
% константы из условия
c.A = 1;
c.B = 1;
c.T = 3;
c.U0 = 1;
c.Y0 = 0;
c.L0 = 1;
% шаг сетки для времени
c.DT = 0.01;
% начальное приближение для численного алгоритма поиска корней
c.Z0 = [0 0 1];
% сетка по времени
c.TT = 0:c.DT:c.T;
% вектор начальных условий
c.Y = [c.Y0 0 0 0 0 0 0 0];
% константы h1, h2, h3
c.H = [
  2 / c.A * (c.T.^2 + 6 * c.T / c.A + 12 / c.A.^2)
  2 / c.A * (c.T - 2 / c.A)
  1 / c.A * (c.T.^2 - 6 * c.T / c.A + 12 / c.A.^2)
];
```

Потом определяем вспомогательные функции (здесь у всех функци последний параметр -- структура констант `c`, заданая выше):

```matlab
function res = b_eval(t, c)
% вычисление b2, b3, b4
  res = [
    c.H(1) / c.A * exp(c.A * (t - c.T)) - 4 / c.A * t.^3 + 3 * c.H(2) * t.^2 - 2 * c.H(3) * (t + 1 / c.A)
    -c.H(1) / c.A.^2 * exp(c.A * (t - c.T)) + t.^4 / c.A - c.H(2) * t.^3 + c.H(3) * (t.^2 + t / c.A + 2 / c.A.^2)
    c.H(1) * exp(c.A * (t - c.T)) - 12 * t.^2 / c.A + 6 * c.H(2) * t - 2 * c.H(3)
  ];
end

function res = p1_eval(t, z, c)
% вычисление p_1(t)
  b = b_eval(t, c);
  res = z(1) * b(1) + z(2) * b(2) + z(3) * b(3);
end

function res = u_eval(t, z, c)
% вычисление u(t)
  p1 = p1_eval(t, z, c);
  res = c.U0 * sign(c.B * p1);
end
```

Мы задаем функцию $`\mathbf{f}(t, \mathbf{y}(t), \mathbf{z})`$, возвращающую правую часть системы дифуров в `equation(y, t, z, c)`:

```matlab
function y_dot = equations(y, t, z, c)
  u = u_eval(t, z, c);
  y_dot = [
    -c.A * y(1) + c.B * u
    6 * y(4) - 4 * t * y(5)
    2 * t * y(5) - 2 * y(7)
    -2 * t.^2 * y(1) + 12 * y(6) - 4 * t * y(8)
    t.^2 * y(1)
    t * y(1)
    t.^3 * y(1)
    y(1)
    ];
end
```

функция ошибок $`\mathbf{r}(\mathbf{x}, \mathbf{z})`$ задается так:

```matlab
function res = residue(x, z, c)
  p1 = p1_eval(c.T, z, c);
  res = [
    x(3) - x(4)
    p1 - 2 * c.L0 * y(2)
    z(3) + z(2) - 2 * c.L0 * y(3)
    ];
end
```

затем определяем функцию для численного интегрирования системы $`\tilde{\mathbf{y}}(t, \mathbf{y}_0)`$:

```matlab
function res = integrate_system(z, c)
  [res, ~, ~] = lsode(@(y, t) equations(y, t, z, c), c.Y, c.TT);
end
```

здесь начальные условия $`\mathbf{y}_0`$ заданы в условии, поэтому мы передаем их как часть констант `c.Y`. Запись `@(a, b) function(a, b, c)` фиксирует третий параметр `c` данной функции на каком-то известном значений и превращает ее в функцию двух параметров, здесь этот синтаксис используется, чтобы зафиксировать не зависящие от времени параметры `z` и `c`.

Осталось собрать все вместе и записать функцию $`\mathbf{g}(\mathbf{z})`$, корни которой будем искать:

```matlab
function res = ftarget(z, c)
  % интегриуем систему
  y = integrate_system(z, c);
  y_T = y(end,:);
  % считаем ошибки
  res = residue(y_T, z, c);
end
```

и записываем основной код решения уравнения при помощи стандартной функции `fsolve`:

```matlab
z_opt = fsolve(@(z) ftarget(z, c), c.Z0);
```

осталось вычислить $`\mathbf{y}`$ при оптимальных параметрах и построить графики:

```matlab
y_opt = integrate_system(z_opt, c);
% время переключения
sigma = fsolve(@(t) p1_eval(t, z_opt, c), c.T/2);

% графики y_i(t), i = 1:8
figure;

for k = 1:8
  subplot(4, 2, k);
  plot(c.TT, y_opt(:, k));
  title(sprintf('y_%d(t)', k));
end
%

% значения b2, b3, b4 на сетке
b_opt = cell2mat(arrayfun(@(t) b_eval(t, c), c.TT, 'UniformOutput', false));

% график b2, b3, b4
figure;
for k = 1:3
  subplot(3, 1, k);
  plot(c.TT, b_opt(k, :));
  title(sprintf('b_%d(t)', k + 1));
end
%

% график p_1(t)
p1_opt = arrayfun(@(t) p1_eval(t, z_opt, c), c.TT);

figure;
plot(c.TT, p_opt);
title('p_1(t)');
%

% график u(t)
u_opt = arrayfun(@(t) u_eval(t, z_opt, c), c.TT);

figure;
plot(c.TT, u_opt);
title('u(t)');
%
```